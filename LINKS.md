[Курс в видеоформате](https://youtube.com/playlist?list=PLisqB92_b4TlQH3jVGf6lrFMVqalCTjAQ)

[Курс в виде книги](https://glr.doatta.cloud)

Можно скачать pdf и md файлы с [Gitlab](https://gitlab.com/doatta/gnu-linux-rhcsa) и [Github](https://github.com/doatta/gnu-linux-rhcsa)

[Телеграмм группа](https://t.me/gnuslashlinux) для вопросов и общения

Если заметите опечатки/ошибки, а также если у вас будут вопросы и предложения - можете открывать issue или писать в телеграмме.

[Все темы экзамена](https://www.redhat.com/en/services/training/ex200-red-hat-certified-system-administrator-rhcsa-exam?section=Objectives)

---

### TODO

##### Темы экзамена, которые осталось разобрать:

- Use grep and regular expressions to analyze text

- Adjust process scheduling. Manage tuning profiles

- Configure IPv6 addresses

- Securely transfer files between systems. Mount and unmount network file systems using NFS. star

- Manage layered storage

- Configure time service clients

- Find and retrieve container images from a remote registry. Inspect container images. Perform container management using commands such as podman and skopeo. Perform basic container management such as running, starting, stopping, and listing running containers. Run a service inside a container. Configure a container to start automatically as a systemd service. Attach persistent storage to a container.

```Однако курс не только ради экзамена, поэтому будут и другие темы```

##### Составить много-много практических задач
